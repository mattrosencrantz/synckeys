package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"os/user"
	"path/filepath"
	"time"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/gitlab"
)

const api = "https://gitlab.com/api/v4/user/keys"

var ports = []int16{
	31332,
	23432,
	14532,
}

type sshKey struct {
	ID        int64     `json:"id"`
	Title     string    `json:"title"`
	Key       string    `json:"key"`
	CreatedAt time.Time `json:"created_at"`
}

type config struct {
	User       string        `json:"user"`        // The user on whose behalf we are running.
	Auth       *oauth2.Token `json:"token"`       // Oauth2 token for talking to gitlab.
	UpdateTime time.Time     `json:"update_time"` // Last time ssh keys were checked.
	user       *user.User
}

func getConfig(u *user.User) (*config, error) {
	c := config{user: u}
	data, err := ioutil.ReadFile(c.path())
	if os.IsNotExist(err) {
		return &c, nil
	} else if err != nil {
		return nil, err
	}
	return &c, json.Unmarshal(data, &c)
}

func (c *config) path() string {
	return filepath.Join(c.user.HomeDir, ".config", "synckeys.json")
}

func (c *config) publicKey() string {
	return c.privateKey() + ".pub"
}

func (c *config) privateKey() string {
	return filepath.Join(c.user.HomeDir, ".ssh", "id_ed25519")

}

func (c *config) authorizedKeys() string {
	return filepath.Join(c.user.HomeDir, ".ssh", "authorized_keys")
}

func (c *config) Token() (*oauth2.Token, error) {
	log.Println("token")
	var err error
	ctx := context.Background()
	conf := &oauth2.Config{
		ClientID:     "042794ed325e3faa210b17313d35464b16205266b5d869f368000292da402a0f",
		ClientSecret: "63f3ba7ff71ebdcb80e48b241770df1166240799a0a179dd2fe4cf8a4997d418",
		Scopes:       []string{"api"},
		Endpoint:     gitlab.Endpoint,
	}
	var token *oauth2.Token
	if c.Auth == nil {
		log.Println("Creating new auth token")
		// The saved token is empty, get the user to authorize.
		codeChan := make(chan string)
		http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			w.Write([]byte("<script>close();</script>"))
			codeChan <- r.URL.Query().Get("code")
		})
		var redirectURL string
		var srv *http.Server
		var ln net.Listener
		for _, p := range ports {
			srv = &http.Server{Addr: fmt.Sprintf(":%d", p)}
			ln, err = net.Listen("tcp", srv.Addr)
			if err != nil {
				continue
			}
			redirectURL = fmt.Sprintf("http://localhost:%d", p)
		}
		conf.RedirectURL = redirectURL
		if err != nil {
			return nil, err
		}
		go func() {
			srv.Serve(ln)
		}()
		// Call the oath provider.
		url := conf.AuthCodeURL("state", oauth2.AccessTypeOffline)
		if err := exec.Command("xdg-open", url).Start(); err != nil {
			fmt.Printf("Open in browser: %s", url)
		}

		code := <-codeChan
		srv.Shutdown(ctx)

		token, err = conf.Exchange(ctx, code)
	} else {
		log.Println("Reusing existing auth token")
		token, err = conf.TokenSource(ctx, c.Auth).Token()
	}
	if err == nil && (c.Auth == nil || *c.Auth != *token) {
		log.Println("Auth-token updated, writing config.")
		c.Auth = token
		c.write()
	}
	return c.Auth, err
}

func (c *config) checkLocalKey(client *http.Client, keys []sshKey) (bool, error) {
	changed := false
	key, err := ioutil.ReadFile(c.publicKey())
	if os.IsNotExist(err) {
		var host string
		if host, err = os.Hostname(); err != nil {
			return changed, err
		}
		fmt.Println("Creating local key")
		cmd := exec.Command("ssh-keygen", "-t", "ed25519", "-C", host, "-f", c.privateKey(), "-N", "")
		cmd.Stderr = os.Stderr
		cmd.Stdout = os.Stdout
		if err = cmd.Run(); err != nil {
			return changed, err
		}
		key, err = ioutil.ReadFile(c.publicKey())
	}
	if err != nil {
		return false, err
	}
	keyStr := string(key)
	fields := bytes.Fields(key)
	title := ""
	if l := len(fields); l < 2 || l > 3 {
		return false, fmt.Errorf("Could not parse key %s: %q", c.publicKey(), keyStr)
	} else if l == 3 {
		title = string(fields[2])
	}
	log.Printf("Found local key %q: %q", title, keyStr)
	found := false
	for _, k := range keys {
		keyMatch := k.Key == keyStr
		titleMatch := k.Title == title
		if keyMatch && titleMatch {
			found = true
		} else if keyMatch || titleMatch {
			// delete key
			changed = true
			req, err := http.NewRequest(http.MethodDelete,
				fmt.Sprintf("%s/%d", api, k.ID), nil)
			if err != nil {
				return changed, err
			}
			log.Printf("Deleting key: %+v", k)
			if _, err := client.Do(req); err != nil {
				return changed, err
			}
		}
	}
	if !found {
		// add key
		changed = true
		values := url.Values{}
		values.Add("title", title)
		values.Add("key", keyStr)
		log.Printf("Creating key: %q: %q", title, keyStr)
		_, err = client.PostForm(api, values)
	}
	return changed, err
}

func getJSON(fn func() (*http.Response, error), data interface{}) error {
	resp, err := fn()
	if err != nil {
		return err
	}
	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	return json.Unmarshal(content, data)
}

func (c *config) updateKeys(client *http.Client) (err error) {
	log.Printf("Fetching updated keys")
	fn := func() (*http.Response, error) { return client.Get(api) }
	var keys []sshKey
	if err = getJSON(fn, &keys); err != nil {
		return err
	}
	if changed, err := c.checkLocalKey(client, keys); err != nil {
		return err
	} else if changed {
		keys = keys[:0]
		if err = getJSON(fn, &keys); err != nil {
			return err
		}
	}
	log.Println("Keys updated, writing config.")
	var f *os.File
	f, err = os.OpenFile(c.authorizedKeys(), os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		return err
	}
	for _, k := range keys {
		if _, err = fmt.Fprintln(f, k.Key, " ", k.Title); err != nil {
			return err
		}
	}
	c.UpdateTime = time.Now()
	if err = c.write(); err != nil {
		return err
	}
	return nil
}

func (c *config) write() error {
	data, err := json.Marshal(c)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(c.path(), data, 0600)
}

func main() {
	var err error
	var u *user.User
	var c *config

	if u, err = user.Current(); err != nil {
		panic(err)
	}
	if c, err = getConfig(u); err != nil {
		panic(err)
	}
	if time.Now().Before(c.UpdateTime.Add(time.Minute * 10)) {
		// It hasn't been long since the last update, ignore.
		return
	}
	client := oauth2.NewClient(context.Background(), c)
	// Ask for an updated set of keys from gitlab.
	if err = c.updateKeys(client); err != nil {
		panic(err)
	}
}
